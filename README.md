# Feapi

[![Build Status](https://travis-ci.org/getcc/feapi.svg?branch=master)](https://travis-ci.org/getcc/feapi)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/getcc/feapi/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/getcc/feapi/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/getcc/feapi/badge.svg?branch=master)](https://coveralls.io/github/getcc/feapi?branch=master)

[![Packagist](https://img.shields.io/packagist/v/getcc/feapi.svg)](https://packagist.org/packages/getcc/feapi)
[![Packagist](https://poser.pugx.org/getcc/feapi/d/total.svg)](https://packagist.org/packages/getcc/feapi)
[![Packagist](https://img.shields.io/packagist/l/getcc/feapi.svg)](https://packagist.org/packages/getcc/feapi)

Package description: CHANGE ME

## Installation

Install via composer
```bash
composer require getcc/feapi
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Getcc\Feapi\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Getcc\Feapi\Facades\Feapi::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Getcc\Feapi\ServiceProvider" --tag="config"
```

## Usage

CHANGE ME

## Security

If you discover any security related issues, please email 
instead of using the issue tracker.

## Credits

- [](https://github.com/getcc/feapi)
- [All contributors](https://github.com/getcc/feapi/graphs/contributors)

This package is bootstrapped with the help of
[melihovv/laravel-package-generator](https://github.com/melihovv/laravel-package-generator).
