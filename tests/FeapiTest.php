<?php

namespace Getcc\Feapi\Tests;

use Getcc\Feapi\Facades\Feapi;
use Getcc\Feapi\ServiceProvider;
use Orchestra\Testbench\TestCase;

class FeapiTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'feapi' => Feapi::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
