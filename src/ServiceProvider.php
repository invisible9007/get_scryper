<?php

namespace Getcc\Feapi;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/feapi.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('feapi.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'feapi'
        );

        $this->app->bind('feapi', function () {
            return new Feapi();
        });

        $this->app->bind('jshapi', function () {
            return new JshApi();
        });
    }
}
