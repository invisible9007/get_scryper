<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class Check extends ApiRequest
{
    private $spider = 'fecurlcheck';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        // if (!empty($resp->items)) {
        //     return $resp->items[0];
        // }
        //
        // if "checkstatus": "ID doesn't match any card in database"
        //  that means this is double check ... should carefull with this ... dont retry a failed job .solve  fail job manually
        return $resp;
    }

    public function getParams()
    {
        return [
            'url'      => $this->getFeurls() . 'store/index.php?id=billing',
            'meta'     => [
                'cid'     => $this->getCid(),
                'uid'     => $this->getFeuser()->data['uid'],
                'cookies' => $this->getFeuser()->data['cookies'],
                // 'user' => $this->getFeuser()->username,
                // 'pass' => $this->getFeuser()->password,
                // 'captcha' => $this->getFeuser()->data['captcha'],
                // 'proxy' => 'http://' . $this->getProxy(),
            ],
            'cookies'  => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'callback' => 'check_card',
        ];
    }

}
