<?php
namespace Getcc\Feapi\Client\Fe;

// set_time_limit(0);
use Getcc\Feapi\Api as ApiRequest;

class Multibuy extends ApiRequest
{
    private $spider = 'fecurlmultibuy';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        // if (empty($response->items)) {
        //     $showbought = Feapi::get('showbought');
        //     $showbought->setCid($this->getCid());
        //     $showbought->setFelogin($this->getFeuser());
        //     $showbought->setServer($this->getServer());
        //     $response = $showbought->request();
        //     dd($response);
        // }

        // GuzzleHttp/Exception/ConnectException with message 'cURL error 52: Empty reply from server (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)'
        return $response;
    }

    public function getParams()
    {
        return [
            'url'         => $this->getFeurls() . 'store/index.php?id=cart',
            'meta'        => [
                'cid'     => $this->getCid(),
                'uid'     => $this->getFeuser()->data['uid'],
                'cookies' => $this->getFeuser()->data['cookies'],
                // 'user' => $this->getFeuser()->username,
                // 'pass' => $this->getFeuser()->password,
                // 'captcha' => $this->getFeuser()->data['captcha'],
                // 'proxy' => 'http://' . $this->getProxy(),
            ],
            'cookies'     => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'callback'    => 'add_carts',
            'dont_filter' => 'True',
        ];
    }

}
