<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class Buy extends ApiRequest
{
    private $spider = 'fecurlbuy';
    // private $url = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        if (!empty($resp->items)) {
            return $resp->items[0];
        }
        return $resp;
    }

    public function getParams()
    {
        return [
            'url'      => $this->getFeurls() . 'store/index.php?id=cart',
            'meta'     => [
                'cid'     => $this->getCid(),
                'uid'     => $this->getFeuser()->data['uid'],
                'cookies' => $this->getFeuser()->data['cookies'],
                'user'    => $this->getFeuser()->username,
                'pass'    => $this->getFeuser()->password,
                'captcha' => $this->getFeuser()->data['captcha'],
                // 'proxy' => 'http://' . $this->getProxy(),
            ],
            'callback' => 'startbuy',
        ];
    }

}
