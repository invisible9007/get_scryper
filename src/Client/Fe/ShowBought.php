<?php

namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class ShowBought extends ApiRequest
{
    private $spider = 'feopenbought';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {
        return [

            'url'      => $this->getFeurls() . 'store/index.php?id=billing',
            'cookies'  => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'meta'     => [
                'cid' => $this->getCid(),
            ],
            'callback' => 'open_now',
        ];
    }

}
