<?php
namespace Getcc\Feapi\Client\Fe;

use Cache;
use Getcc\Feapi\Api as ApiRequest;

class Felogin extends ApiRequest
{
    private $spider = 'fecurl_login';
    private $url    = '/crawl.json';
    private $user;
    private $pass;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        $this->Cached($this->user, $response->items);
        return $response;
    }

    public function setLogin($feuserInfo)
    {
        $this->user = $feuserInfo->username;
        $this->pass = $feuserInfo->password;
        return $this->request();
    }

    public function getParams()
    {
        $params = [
            'url'      => $this->getFeurls(),
            'meta'     => [
                'user' => $this->user,
                'pass' => $this->pass,
                // 'proxy' => 'http://' . $this->getProxy(),
            ],
            'callback' => 'start_login',
        ];
        return $params;
    }

    public function Cached($tag, $item)
    {
        $minutes = now()->addMinutes(3000);
        Cache::tags(['login', 'response'])->put($tag, $item, $minutes);
    }

}
