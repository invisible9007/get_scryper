<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class FeJscheck extends ApiRequest
{
    private $spider = 'fespider';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        // if (!empty($resp->items)) {
        //     return $resp->items[0];
        // }
        return $resp;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getFeurls(),
            'callback'    => 'start',
            'dont_filter' => 'True',
        ];
        return $params;
    }

}
