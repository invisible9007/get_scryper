<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class PreorderBaseFilter extends ApiRequest
{
    private $spider = 'feprebasefilter';
    private $url    = '/crawl.json';

    private $bin;
    private $baseslist;
    private $lastPage;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {
        return [
            'url'     => $this->getFeurls(),
            'meta'    => [
                'bin'       => $this->bin,
                'baseslist' => $this->baseslist,
                'lastPage'  => $this->lastPage,
            ],
            'cookies' => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
        ];
    }

    public function setBin($bin)
    {
        $this->bin = $bin;
    }

    public function setBaseslist($baseslist)
    {
        $this->baseslist = $baseslist;
    }

    public function setLastpage($lastPage)
    {
        $this->lastPage = $lastPage;
    }
}
