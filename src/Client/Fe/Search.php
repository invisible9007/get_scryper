<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class Search extends ApiRequest
{
    private $spider = 'fecurlfilter';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    //

    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $response;
    }

    public function getParams()
    {
        $params = [

            'url'     => $this->getFeurls() . 'store/index.php?' . $this->getQuery(),
            'cookies' => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            // 'meta' => [
            //     'proxy' => 'http://' . $this->getProxy(),
            // ],
        ];
        return $params;
    }

}
