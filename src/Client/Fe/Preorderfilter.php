<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class Preorderfilter extends ApiRequest
{
    private $spider = 'fepreorder';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }
    //

    public function request()
    {
        return $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
    }

    public function getParams()
    {
        $params = [
            'url'     => $this->getFeurls(),
            'cookies' => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'meta'    => [
                'binlist' => $this->getBins(),
            ],
        ];
        return $params;
    }

}
