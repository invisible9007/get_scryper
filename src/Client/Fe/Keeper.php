<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class Keeper extends ApiRequest
{
    private $spider = 'fekeeper';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        // if (!empty($resp->items)) {
        //     return $resp->items[0];
        // }
        return $resp;
    }

    public function getParams()
    {
        $params = [
            'url'     => $this->getFeurls() . 'store/index.php?id=cvvs',
            // 'meta' => [
            //     'proxy' => 'http://' . $this->getProxy(),
            // ],
            'cookies' => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
        ];
        return $params;
    }

}
