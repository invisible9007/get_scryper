<?php
namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class ClearCart extends ApiRequest
{
    private $spider = 'clearcart';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $response;
    }

    public function getParams()
    {
        return [
            'url'         => $this->getFeurls() . 'store/index.php?id=cart',
            'meta'        => [
                'uid'     => $this->getFeuser()->data['uid'],
                'cookies' => $this->getFeuser()->data['cookies'],
            ],
            'cookies'     => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'callback'    => 'clearing_cart',
            'dont_filter' => 'True',
        ];
    }
}
