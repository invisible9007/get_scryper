<?php

namespace Getcc\Feapi\Client\Fe;

use Getcc\Feapi\Api as ApiRequest;

class OpenBuy extends ApiRequest
{
    /**
     * @var string
     */
    private $spider = 'feopenbought';
    private $url    = '/crawl.json';

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());

        // if (!empty($resp->items)) {
        //     return $resp->items[0];
        // }
        return $resp;
    }

    public function getParams()
    {
        return [
            'url'      => $this->getFeurls() . 'store/index.php?id=showcc&c=' . $this->getCid(),
            'cookies'  => [
                'php_session_id_real' => $this->getFeuser()->data['cookies'],
            ],
            'meta'     => [
                'cid' => $this->getCid(),
                // 'proxy' => 'http://' . $this->getProxy(),
            ],
            'callback' => 'open_now',
        ];
    }

}
