<?php

namespace Getcc\Api\Client;

use Http\Client\HttpAsyncClient;
use Http\Client\RequestFactory;
use Http\Discovery\HttpAsyncClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Psr\Http\Message\ResponseInterface;

abstract class ClientAsync
{
    /**
     * @var mixed
     */
    protected $httpAsyncClient;

    /**
     * @var mixed
     */
    protected $messageFactory;

    /**
     * @var mixed
     */
    protected $uriFactory;

    /**
     * @param HttpAsyncClient    $httpAsyncClient
     * @param nullRequestFactory $requestFactory
     */
    public function __construct(HttpAsyncClient $httpAsyncClient = null, RequestFactory $requestFactory = null)
    {
        $this->httpAsyncClient = $httpAsyncClient ?: HttpAsyncClientDiscovery::find();
        $this->requestFactory = $requestFactory ?: MessageFactoryDiscovery::find();
    }

    /**
     * @param  $url
     * @param  array $headers
     * @return mixed
     */
    protected function getResponse($url, array $params = [])
    {
        $promise = $this->httpAsyncClient->sendAsyncRequest($this->buildRequest($url, $params));
        return $promise->then(
            // The success callback
            function (ResponseInterface $response) {
                echo 'Yay, we have a shiny new response!';

                // Write status code to some log file
                file_put_contents('responses.log', $response->getStatusCode() . "\n", FILE_APPEND);

                return $response;
            },
            // The failure callback
            function (\Exception $exception) {
                echo 'Oh darn, we have a problem';

                throw $exception;
            }
        );
    }

    /**
     * @param  $url
     * @param  array $params
     * @return mixed
     */
    protected function request($url, array $params = [])
    {
        return $this->getResponse($url, $params);
    }

    /**
     * @param  $url
     * @param  array $params
     * @return mixed
     */
    private function buildRequest($url, array $params = [])
    {
        return $this->requestFactory->createRequest('POST', $url, $params);
    }
}
