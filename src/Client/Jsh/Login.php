<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class Login extends ApiRequest
{
    private $spider = 'login';
    private $url    = '/crawl.json';
    private $user;
    private $pass;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getJshopUrls() . "login/",
            'meta'        => [
                'user' => $this->user,
                'pass' => $this->pass,
            ],
            'dont_filter' => "True",
            'callback'    => "start_login",
        ];
        return $params;
    }

    /**
     * { Get result of pending check }
     *
     * @param      <str>  $cookies   The cookies
     * @param      <str>  $cid       The cid
     * @param      <int>  $order_id  The order_id of that cid
     */
    public function input($jshUser)
    {
        $this->user = $jshUser->username;
        $this->pass = $jshUser->password;
    }

}
