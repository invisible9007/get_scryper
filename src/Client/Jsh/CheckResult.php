<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class CheckResult extends ApiRequest
{
    private $spider = 'checkResult';
    private $url    = '/crawl.json';
    private $cid;
    private $order_id;
    private $cookies;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getJshopUrls() . 'core/ajax/clients/',
            'cookies'     => $this->cookies,
            'meta'        => [
                'cid'      => $this->cid,
                'order_id' => $this->order_id,
            ],
            "dont_filter" => "True",
            "callback"    => "reload_status",
        ];
        return $params;
    }

    /**
     * { Get result of pending check }
     *
     * @param      <str>  $cookies   The cookies
     * @param      <str>  $cid       The cid
     * @param      <int>  $order_id  The order_id of that cid
     */
    public function input($cookies, $cid, $order_id)
    {
        $this->cid      = $cid;
        $this->order_id = $order_id;
        $this->cookies  = $cookies;
    }

}
