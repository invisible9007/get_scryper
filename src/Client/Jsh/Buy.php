<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class Buy extends ApiRequest
{
    private $spider = 'buy';
    private $url    = '/crawl.json';
    private $cart_cid;
    private $cookies;

    public function __construct()
    {
        parent::__construct();
    }

    // GuzzleHttp/Exception/ConnectException with message 'cURL error 52: Empty reply from server (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)'
    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $response;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getJshopUrls() . 'core/ajax/clients/',
            'cookies'     => $this->cookies,
            'meta'        => [
                'cart_cid' => $this->cart_cid,
            ],
            "dont_filter" => "True",
            "callback"    => "cart",
        ];
        return $params;
    }

    public function input($cookies, $cart_cid)
    {
        $this->cart_cid = $cart_cid;
        $this->cookies  = $cookies;
    }

}
