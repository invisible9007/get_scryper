<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class Check extends ApiRequest
{
    private $spider = 'check';
    private $url    = '/crawl.json';
    private $cid;
    private $cookies;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {

        $params = [
            'url'         => $this->getJshopUrls() . 'core/ajax/clients/',
            'cookies'     => $this->cookies,
            'meta'        => [
                'cid' => $this->cid,
            ],
            "dont_filter" => "True",
            "callback"    => "start_check",
        ];
        return $params;
    }

    public function input($cookies, $cid)
    {
        $this->cid     = $cid;
        $this->cookies = $cookies;
    }

}
