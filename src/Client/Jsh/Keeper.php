<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class Keeper extends ApiRequest
{
    private $spider = 'keeper';
    private $url    = '/crawl.json';
    private $cookies;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $resp = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $resp;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getJshopUrls() . 'core/ajax/clients/',
            'cookies'     => $this->cookies,
            "dont_filter" => "True",
            "callback"    => "grab_data",
        ];
        return $params;
    }

    public function input($cookies)
    {
        $this->cookies = $cookies;
    }

}
