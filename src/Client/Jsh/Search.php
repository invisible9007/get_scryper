<?php
namespace Getcc\Feapi\Client\Jsh;

use Getcc\Feapi\Api as ApiRequest;

class Search extends ApiRequest
{
    private $spider = 'filter';
    private $url    = '/crawl.json';
    private $field;
    private $page;
    private $cookies;

    public function __construct()
    {
        parent::__construct();
    }

    public function request()
    {
        $response = $this->sendRequest('/crawl.json', $this->spider, $this->getParams());
        return $response;
    }

    public function getParams()
    {
        $params = [
            'url'         => $this->getJshopUrls() . 'core/ajax/cards/',
            'cookies'     => $this->cookies,
            'meta'        => [
                'init'  => '0',
                'field' => $this->field,
                'page'  => $this->page,
            ],
            "dont_filter" => "True",
            "callback"    => "cvvFilter",
        ];
        return $params;
    }

    public function input($cookies, $field = "cc_type##all", $page = '1')
    {
        $this->field   = $field;
        $this->page    = $page;
        $this->cookies = $cookies;
    }

}
