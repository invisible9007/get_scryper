<?php
namespace Getcc\Feapi\Parser;

use Cache;

class LoginsResponse extends Base
{
    /**
     * @var mixed
     */
    private $_created;
    /**
     * @var mixed
     */
    private $_modified;
    /**
     * @var mixed
     */
    public $balance;

    /**
     * @var mixed
     */
    public $cookies;

    /**
     * @var mixed
     */
    public $lastlogin;

    /**
     * @var mixed
     */
    public $uid;

    /**
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param array $item
     */
    public function InfoCached(
              $tag,
        array $item
    ) {
        $minutes = now()->addMinutes(30);
        \Cache::tags([$tag, 'login'])->put($tag, $john, $minutes);
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return mixed
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @return mixed
     */
    public function getLastlogin()
    {
        return $this->lastlogin;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }
}
