<?php
namespace Getcc\Feapi\Parser;

class BuyResponse extends Base
{
    /**
     * @var mixed
     */
    public $address;

    /**
     * @var mixed
     */
    public $basename;

    /**
     * @var mixed
     */
    public $boughtdate;

    /**
     * @var mixed
     */
    public $buyerproxyid;

    /**
     * @var mixed
     */
    public $cardnum;

    /**
     * @var mixed
     */
    public $checkstatus;

    /**
     * @var mixed
     */
    public $cid;

    /**
     * @var mixed
     */
    public $city;

    /**
     * @var mixed
     */
    public $country;

    /**
     * @var mixed
     */
    public $cvv;

    /**
     * @var mixed
     */
    public $czip;

    /**
     * @var mixed
     */
    public $exp;

    /**
     * @var mixed
     */
    public $name;

    /**
     * @var mixed
     */
    public $phone;

    /**
     * @var mixed
     */
    public $price;

    /**
     * @var mixed
     */
    public $shopbuyer;

    /**
     * @var mixed
     */
    public $state;

    /**
     * @var mixed
     */
    public $timeofchk;

    /**
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getBasename()
    {
        return $this->basename;
    }

    /**
     * @return mixed
     */
    public function getBoughtdate()
    {
        return $this->boughtdate;
    }

    /**
     * @return mixed
     */
    public function getBuyerproxyid()
    {
        return $this->buyerproxyid;
    }

    /**
     * @return mixed
     */
    public function getCardnum()
    {
        return $this->cardnum;
    }

    /**
     * @return mixed
     */
    public function getCheckstatus()
    {
        return $this->checkstatus;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * @return mixed
     */
    public function getCzip()
    {
        return $this->czip;
    }

    /**
     * @return mixed
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getShopbuyer()
    {
        return $this->shopbuyer;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getTimeofchk()
    {
        return $this->timeofchk;
    }

    /**
     * @param $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param $basename
     */
    public function setBasename($basename)
    {
        $this->basename = $basename;
    }

    /**
     * @param $boughtdate
     */
    public function setBoughtdate($boughtdate)
    {
        $this->boughtdate = $boughtdate;
    }

    /**
     * @param $buyerproxyid
     */
    public function setBuyerproxyid($buyerproxyid)
    {
        $this->buyerproxyid = $buyerproxyid;
    }

    /**
     * @param $cardnum
     */
    public function setCardnum($cardnum)
    {
        $this->cardnum = $cardnum;
    }

    /**
     * @param $checkstatus
     */
    public function setCheckstatus($checkstatus)
    {
        $this->checkstatus = $checkstatus;
    }

    /**
     * @param $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    /**
     * @param $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @param $cvv
     */
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;
    }

    /**
     * @param $czip
     */
    public function setCzip($czip)
    {
        $this->czip = $czip;
    }

    /**
     * @param $exp
     */
    public function setExp($exp)
    {
        $this->exp = $exp;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param $shopbuyer
     */
    public function setShopbuyer($shopbuyer)
    {
        $this->shopbuyer = $shopbuyer;
    }

    /**
     * @param $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @param $timeofchk
     */
    public function setTimeofchk($timeofchk)
    {
        $this->timeofchk = $timeofchk;
    }
}
