<?php
namespace Getcc\Feapi\Parser;

class SearchResponse extends Base
{

    /**
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @return mixed
     */
    public function getChecktime()
    {
        return $this->checktime;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getExp()
    {
        return $this->exp;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @param $card
     */
    public function setCard($card)
    {
        $this->card = $card;
    }

    /**
     * @param $checktime
     */
    public function setChecktime($checktime)
    {
        $this->checktime = $checktime;
    }

    /**
     * @param $cid
     */
    public function setId($cid)
    {
        $this->id = $cid;
    }

    /**
     * @param $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @param $czip
     */
    public function setZip($czip)
    {
        $this->zip = $czip;
    }

    /**
     * @param $exp
     */
    public function setExp($exp)
    {
        $this->exp = $exp;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
    /**
     * @param $state
     */
    public function setIso($state)
    {
        $this->iso = $iso;
    }

    /*    public function __construct($base, $card, $checktime, $cid, $city, $country, $czip, $exp, $name, $price, $state)
{
$this->setBase($base);
$this->setCard($card);
$this->setChecktime($checktime);
$this->setCid($cid);
$this->setCity($city);
$this->setCountry($country);
$this->setCzip($czip);
$this->setExp($exp);
$this->setName($name);
$this->setPrice($price);
$this->setState($state);
}*/
}
