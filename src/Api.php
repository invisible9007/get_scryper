<?php

namespace Getcc\Feapi;

// set_time_limit(0);
use Cache;
use GuzzleHttp\Client;

class Api
{
    private $cardid;
    private $bins;
    private $client;
    private $userid;
    private $feuser;
    private $server;
    private $query;
    private $url = 'http://127.0.0.1:9080';
    // private $url;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => $this->url,
            ]
        );
    }

    /**
     * @param  $method
     * @param  array       $params
     * @throws Exception
     * @return object
     */
    public function sendRequest($url, $spider, $request = [])
    {
        if ($this->server) {
            $this->client = new Client(
                [
                    'base_uri' => $this->server,
                ]
            );
        }
        // dd($this->client);
        $request = $this->client->request(
            'POST',
            $url,
            [
                'json' => [
                    //'id' => 'curltext',
                    'request'     => $request,
                    'spider_name' => $spider,
                ],
            ]
        );
        $response = json_decode($request->getBody()->getContents());

        if (isset($response->error)) {
            throw new Exception($response->error->message);
        } else {
            return $response;
        }
    }

    public function getRequest($method, $params = [])
    {
        $request  = $this->client->request('GET', $method);
        $response = json_decode($request->getBody()->getContents());

        if (isset($response->error)) {
            throw new \Exception($response->error->message);
        } else {
            return $response;
        }

    }

    // Global method , getter setter
    protected function getFeurls()
    {
        // This should be add from confing
        $url = 'http://www.fe-acc18.ru/';
        return $url;
    }

    protected function getJshopUrls()
    {
        // This should be add from confing
        $url = 'https://jshop-biz.cc/';
        return $url;
    }

    protected function getProxy()
    {
        $metas      = explode("\n", file_get_contents(storage_path() . '/proxy/1.txt'));
        $collection = collect($metas);
        $proxy      = $collection->random();
        // $proxy = 'http://200.107.35.154:53281';
        return $proxy;
    }

    public function getUsersid()
    {
        $usersid = Cache::tags(['login', 'response'])->get($this->userid);
        return $usersid[0];
    }

    public function getCid()
    {
        return $this->cardid;
    }

    public function getBins()
    {
        return $this->bins;
    }

    public function getUser()
    {
        return $this->userid;
    }
    public function getQuery()
    {
        return http_build_query($this->query);
    }

    public function getServer()
    {
        return $this->server;
    }

    public function setServer($server)
    {
        $this->server = 'http://' . $server;
    }

    public function setCid($cid)
    {
        $this->cardid = $cid;
    }

    public function setBins($bins)
    {
        $this->bins = $bins;
    }

    public function setUser($user)
    {
        $this->userid = $user;
    }
    public function setFelogin($user)
    {
        $this->feuser = $user;
    }

    public function getFeuser()
    {
        return $this->feuser;
    }

    public function setQuery($input)
    {
        $this->query        = $input;
        $this->query['act'] = 'search';
        $this->query['id']  = 'cvvs';
        return $this;
    }

}
