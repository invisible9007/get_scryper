<?php

namespace Getcc\Feapi;

use Getcc\Feapi\Client\Jsh\Buy;
use Getcc\Feapi\Client\Jsh\Check;
use Getcc\Feapi\Client\Jsh\CheckResult;
use Getcc\Feapi\Client\Jsh\Keeper;
use Getcc\Feapi\Client\Jsh\Login;
use Getcc\Feapi\Client\Jsh\Search;

class JshApi
{
    /**
     * @var array
     */
    protected $api;

    protected static $client = [
        'login'       => Login::class,
        'search'      => Search::class,
        'check'       => Check::class,
        'buy'         => Buy::class,
        'checkResult' => CheckResult::class,
        'keeper'      => Keeper::class,
    ];
    public function keeper()
    {
        $req = $this->api = new Keeper();
        return $req;
    }

    public function login()
    {
        $req = $this->api = new Login();
        return $req;
    }

    public function search()
    {
        $req = $this->api = new Search();
        return $req;
    }

    public function multibuy()
    {
        $req = $this->api = new Buy();
        return $req;
    }

    public function check()
    {
        $req = $this->api = new Check();
        return $req;
    }

    public function checkResult()
    {
        $req = $this->api = new CheckResult();
        return $req;
    }

    /**
     * @param  $method
     * @return mixed
     */
    public static function get($method)
    {
        if (!array_key_exists($method, self::$client)) {
            throw new \InvalidArgumentException("request method=> [$method] not Exits.");
        }

        $request = self::$client[$method];
        if (is_object($request)) {
            return new $request;
        }
        self::$client[$method] = new $request();
        return self::$client[$method];
    }

}
