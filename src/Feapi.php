<?php

namespace Getcc\Feapi;

// set_time_limit(0);
use Getcc\Feapi\Client\Fe\Buy;
use Getcc\Feapi\Client\Fe\Check;
use Getcc\Feapi\Client\Fe\ClearCart;
use Getcc\Feapi\Client\Fe\FeJscheck;
use Getcc\Feapi\Client\Fe\Felogin;
use Getcc\Feapi\Client\Fe\Keeper;
use Getcc\Feapi\Client\Fe\Multibuy;
use Getcc\Feapi\Client\Fe\OpenBuy;
use Getcc\Feapi\Client\Fe\PreorderBaseFilter;
use Getcc\Feapi\Client\Fe\Preorderfilter;
use Getcc\Feapi\Client\Fe\Search;
use Getcc\Feapi\Client\Fe\ShowBought;

class Feapi
{
    /**
     * @var array
     */
    protected $api;

    protected static $client = [
        'felogin'            => Felogin::class,
        'buy'                => Buy::class,
        'openbuy'            => OpenBuy::class,
        'search'             => Search::class,
        'preorderfilter'     => Preorderfilter::class,
        'check'              => Check::class,
        'keeper'             => Keeper::class,
        'showbought'         => ShowBought::class,
        'multibuy'           => Multibuy::class,
        'clearcart'          => ClearCart::class,
        'fejscheck'          => FeJscheck::class,
        'preorderbasefilter' => PreorderBaseFilter::class,
    ];

    /**
     * @return mixed
     */
    public function felogin()
    {
        $req = $this->api = new Felogin();
        return $req;
    }

    public function keeper()
    {
        $req = $this->api = new keeper();
        return $req;
    }

    public function clearcart()
    {
        $req = $this->api = new ClearCart();
        return $req;
    }

    public function search()
    {
        $req = $this->api = new Search();
        return $req;
    }

    public function preorderfilter()
    {
        $req = $this->api = new Preorderfilter();
        return $req;
    }

    public function preorderbasefilter()
    {
        $req = $this->api = new Preorderfilter();
        return $req;
    }

    public function buy()
    {
        $req = $this->api = new Buy();
        return $req;
    }

    public function multibuy()
    {
        $req = $this->api = new Multibuy();
        return $req;
    }

    public function openbuy()
    {
        $req = $this->api = new OpenBuy();
        return $req;
    }

    public function showbought()
    {
        $req = $this->api = new ShowBought();
        return $req;
    }

    public function check()
    {
        $req = $this->api = new Check();
        return $req;
    }

    public function fejscheck()
    {
        $req = $this->api = new FeJscheck();
        return $req;
    }

    /**
     * @param  $method
     * @return mixed
     */
    public static function get($method)
    {
        if (!array_key_exists($method, self::$client)) {
            throw new \InvalidArgumentException("request method=> [$method] not Exits.");
        }

        $request = self::$client[$method];
        if (is_object($request)) {
            return new $request;
        }
        self::$client[$method] = new $request();
        return self::$client[$method];
    }

}
