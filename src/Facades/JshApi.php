<?php

namespace Getcc\Feapi\Facades;

use Illuminate\Support\Facades\Facade;

class JshApi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'jshapi';
    }
}
