<?php

namespace Getcc\Feapi\Facades;

use Illuminate\Support\Facades\Facade;

class Feapi extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'feapi';
    }
}
